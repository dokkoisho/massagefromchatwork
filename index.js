'use strict';

require('dotenv').config();
const mysql = require('mysql');
const axios = require('axios');

const connection = mysql.createConnection({
  host: process.env.HDB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PWD,
  database: process.env.DB_NAME,
});

const API_TOKEN = process.env.API_TOKEN;
const ROOM_ID = process.env.ROOM_ID;
const BASE_ENDPOINT = 'https://api.chatwork.com/v2';
const headerParams = {'X-ChatWorkToken': API_TOKEN};



class MassageFromChatwork {

  async execute() {
    const data = await this.executeGetQuery();
    await this.closeDB();
    const selecedData = await this.selectDataFunc(data);
    await this.send(selecedData);
  }

  async executeGetQuery() {
    return new Promise( (fullfilled, rejected ) => {
      connection.query('select id,page,brand,product,spscore,spfcp,spfmp,pcscore,pcfcp,pcfmp from scores where id in (select max(id) from scores group by page)', (err,results,fields) => {
        if (err) rejected(err);
        fullfilled(results);
      });
    });
  };

  async selectDataFunc(data) {
    let results = '===================================\n【LPの速度check】\n===================================\n';
    for( let i = 0,len = data.length; i < len; i++ ) {
      if( data[i].spfcp >= 4.5 || data[i].spfmp >= 5 || 
          data[i].pcfcp >= 2.5 || data[i].pcfmp >= 3.5
      ){
        results += '\n対象URL: ' + data[i].page + '\nブランド'+ data[i].brand;
        results += '、商品: ' + data[i].product;
        results += '\n\nスマホ '+ data[i].spscore + '点';
        results += '\nFirst Contentful Paint: '+ data[i].spfcp;
        results += '\nFirst Meaningful Paint: '+ data[i].spfmp;
        results += '\nPC'+ data[i].spscore　+ '点';
        results += '\nFirst Contentful Paint: '+ data[i].pcfcp;
        results += '\nFirst Meaningful Paint: '+ data[i].pcfmp + '\n\n';
      }
    }
    return results;
  };

  async send(data) {

    // application/x-www-form-urlencoded形式で送る
    const params = new URLSearchParams();
    params.append('body', data);
    params.append('self_unread', 0);

    axios({
      method: 'POST',
      url: BASE_ENDPOINT + '/rooms/' + ROOM_ID + '/messages',
      headers: headerParams,
      data: params,
    }).then( (res) => {
      if(res.status === 200 && res.statusText =='OK'){
        console.log('message send!');
      }
    });
  }

  async closeDB (){
    connection.end();
  }

}

const obj = new MassageFromChatwork;
obj.execute();
