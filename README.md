# google_speed_batch

- 修正必要なLPの測定データがクリエイティブのchatに送信される。
- 送信先のchatを変更するには ``.env``のROOM_IDを変更する。


## DB情報設定


``Google-speed``や``google-speed-ui`` と同じDBに接続したい。``.env``ファイルに追記。

## 実行

```
node index.js
```